setInterval(function () {
    let time = getCurrentIjedaDateTime();
    document.getElementById("kemoka").innerHTML = time.kemoka;
    document.getElementById("helaka").innerHTML = time.helaka;
    document.getElementById("zzakemo").innerHTML = time.zzakemo;
  }, 20);
  
  function getCurrentIjedaDateTime() {
    const currentUTC = new Date();
    const offset = -4; // UTC-4 timezone offset
  
    const localDate = new Date(currentUTC.getTime() + offset * 60 * 60 * 1000);
  
    const localSeconds =
      localDate.getUTCHours() * 3600 +
      localDate.getUTCMinutes() * 60 +
      localDate.getUTCSeconds() +
      localDate.getMilliseconds() / 1000;
    const zzakemo = Math.floor(localSeconds / 1.728) % 50;
    const helaka = Math.floor(localSeconds / 86.4) % 100;
    const kemoka = Math.floor(localSeconds / 8640) % 10;
  
    return {
      kemoka: kemoka,
      helaka: helaka,
      zzakemo: zzakemo,
    };
  }